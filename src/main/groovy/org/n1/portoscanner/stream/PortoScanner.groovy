package org.n1.portoscanner.stream

import org.n1.portoscanner.MixerChooser
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.sound.sampled.AudioFormat
import javax.sound.sampled.TargetDataLine

/**
 * This class listens to an audio stream from a port and stores any sounds on disk.
 */
class PortoScanner {

    boolean running = true;

    private static final Logger log = LoggerFactory.getLogger(PortoScanner)

    public static void main(String... args) {
        new PortoScanner().run();
    }

    def run() {
        File saveBaseDir = new File("recordings")

        AudioFormat format = MixerChooser.getAudioFormat();

        TargetDataLine line = MixerChooser.getRecordTargetLine();
        line.open(format, line.getBufferSize());

        // play back the captured audio data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int frameSizeInBytes = format.getFrameSize();
        int bufferLengthInFrames = line.getBufferSize() / 8;
        int bufferLengthInBytes = bufferLengthInFrames * frameSizeInBytes;

        SoundStreamProcessor processor = new SoundStreamProcessor(bufferLengthInBytes, format, frameSizeInBytes, saveBaseDir)

        byte[] data = new byte[bufferLengthInBytes];
        int numBytesRead;
        line.start();
        while (running) {
            if ((numBytesRead = line.read(data, 0, bufferLengthInBytes)) == -1) {
                break;
            }
//            log.debug("Processing ${numBytesRead} bytes")
            processor.processSamples(data, numBytesRead);
        }

    }

}
