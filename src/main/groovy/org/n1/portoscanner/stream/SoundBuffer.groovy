package org.n1.portoscanner.stream

/**
 * This class is a container for a buffer of sound samples that is being processed.
 */
class SoundBuffer {

    /** The fixed buffer of samples */
    short[] buffer

    byte[] bytes

    /** Shortcut to buffer.length */
    int length

    /** The start of the unprocessed part of the buffer (shorts) */
    int start

    /** Whether or not this buffer has been completely processed */
    boolean finished = false

    SoundBuffer(short[] buffer, int start, byte[] rawBytes) {
        this.buffer = buffer
        this.start = start
        this.length = buffer.length
        this.bytes = rawBytes

    }
}
