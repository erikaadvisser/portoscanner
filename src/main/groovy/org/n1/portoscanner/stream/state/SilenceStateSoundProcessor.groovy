package org.n1.portoscanner.stream.state

import org.n1.portoscanner.stream.SoundBuffer
import org.n1.portoscanner.stream.SoundProcessingState
import org.n1.portoscanner.stream.SoundStateProcessor
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This class processes the sound stream during a period of silence, detecting the start of sound.
 */
class SilenceStateSoundProcessor implements SoundStateProcessor {

    private static final Logger log = LoggerFactory.getLogger(SilenceStateSoundProcessor)

    /** The amount of samples that have to be higher than (NOISE_MAX + THREE SIGMA) to confirm start of sound) */
    private static final int SOUND_START_TIME_THRESHOLD = 10

    NoiseStatistics noiseStatistics

    int consecutiveSamples = 0

    SilenceStateSoundProcessor(NoiseStatistics noiseStatistics) {
        this.noiseStatistics = noiseStatistics
    }

    @Override
    SoundProcessingState process(SoundBuffer soundBuffer) {
        for (int i = soundBuffer.start; i < soundBuffer.length; i += 1) {
            short correctedValue = soundBuffer.buffer[i] - noiseStatistics.offset
            short amplitude = Math.abs(correctedValue)

            if ( amplitude > noiseStatistics.threeSigma + noiseStatistics.max) {
                consecutiveSamples += 1
                if ( consecutiveSamples >= SOUND_START_TIME_THRESHOLD ) {
                    consecutiveSamples = 0

                    soundBuffer.start = i
                    log.debug("Start of sound detected.")
                    return SoundProcessingState.SOUND
                }
            }
            else {
                consecutiveSamples = 0
            }
        }
        soundBuffer.finished = true
        return SoundProcessingState.SILENCE
    }
}
