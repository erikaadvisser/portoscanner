package org.n1.portoscanner.stream.state

import org.apache.commons.math3.stat.descriptive.SummaryStatistics
import org.n1.portoscanner.stream.SoundBuffer
import org.n1.portoscanner.stream.SoundProcessingState
import org.n1.portoscanner.stream.SoundStateProcessor
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This class processes sound in the start state: when the stream has just started and we need to get a feel for the
 * noise level to be able to cancel it.
 */
class StartStateSoundProcessor implements SoundStateProcessor {

    public static int MINIMUM_SILENCE_SAMPLES = 100000

    private static final Logger log = LoggerFactory.getLogger(StartStateSoundProcessor)

    def stats = new SummaryStatistics()
    int totalSamplesRead = 0

    NoiseStatistics noiseStatistics

    StartStateSoundProcessor(NoiseStatistics noiseStatistics) {
        this.noiseStatistics = noiseStatistics
    }

    @Override
    SoundProcessingState process(SoundBuffer soundBuffer) {
        for (int i = soundBuffer.start; i < soundBuffer.length; i += 1) {
            stats.addValue(soundBuffer.buffer[i])
        }
        soundBuffer.finished = true

        totalSamplesRead += (soundBuffer.length - soundBuffer.start);
        if (totalSamplesRead < MINIMUM_SILENCE_SAMPLES) {
            return SoundProcessingState.START
        }

        if (isSilence()) {
            analyzeNoise()
            log.debug("Noise analyzed. Offset: ${noiseStatistics.offset} threeSigma: ${noiseStatistics.threeSigma} max: ${noiseStatistics.max}")
            return SoundProcessingState.SILENCE
        }
        else {
            log.debug("Noise analyze failed, retrying.")
            totalSamplesRead = 0;
            stats = new SummaryStatistics()
            return SoundProcessingState.START
        }
    }

    boolean analyzeNoise() {
        int sigma = stats.getStandardDeviation()
        int mean = stats.getMean()
        int min = stats.getMin() - mean
        int max = stats.getMax() - mean

        int absMax = Math.max (Math.abs(min), max)

        noiseStatistics.offset= mean
        noiseStatistics.max = absMax
        noiseStatistics.threeSigma = 3 * sigma

        return true
    }

    /**
     * The measured standard deviation for the noise during silence is about 8.5.
     * If the standard deviation is much higher then we probably did not have only silence
     * during the start period.
     *
     * The measured abs max sample value is about 35. This value is also used to detect sound from silence,
     * so we need a realist value for this as well.
     */
    boolean isSilence() {
        int sigma = stats.getStandardDeviation()
        int offset = stats.getMean()
        int min = stats.getMin() - offset
        int max = stats.getMax() - offset
        log.debug("Noise analysis: offset: ${offset}")

        int absMax = Math.max (Math.abs(min), max)
        if ( sigma >= 15) {
            log.debug("Noise analysis fails, sigma too high: ${sigma}")
            return false
        }
        if (absMax >= 50) {
            log.debug("Noise analysis fails, abs max too high: ${absMax}")
            return false
        }
        return true;
    }
}
