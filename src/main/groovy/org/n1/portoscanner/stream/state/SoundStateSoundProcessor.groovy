package org.n1.portoscanner.stream.state

import org.n1.portoscanner.stream.SoundBuffer
import org.n1.portoscanner.stream.SoundProcessingState
import org.n1.portoscanner.stream.SoundStateProcessor
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.sound.sampled.AudioFileFormat
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem
import java.text.SimpleDateFormat

/**
 * This class is the processing state for recording sound.
 */
class SoundStateSoundProcessor implements SoundStateProcessor {

    private static final Logger log = LoggerFactory.getLogger(SilenceStateSoundProcessor)

//    /** 44 khz, so the threshold for detecting silence is 25 ms */
    /** 44.1 khz -> 1 seconds of silence required. */
    private static final int SOUND_END_TIME_THRESHOLD = 44100

    NoiseStatistics noiseStatistics
    AudioFormat format
    int frameSizeInBytes

    int consecutiveSilenceSamples = 0
    File saveDir
    int saveCount = 0
    ByteArrayOutputStream byteOut = new ByteArrayOutputStream()
    def timeFormat = new SimpleDateFormat("YYYY-MM-dd_HH'h'-mm'm'-ss's'");


    SoundStateSoundProcessor(NoiseStatistics noiseStatistics, AudioFormat format, int frameSizeInBytes, File baseDir) {
        this.noiseStatistics = noiseStatistics
        this.format = format
        this.frameSizeInBytes = frameSizeInBytes

        Date now = new Date()
        String dirName = now.format("YYYY-MM-dd_HH'h'-mm'm'-ss's'")

        saveDir = new File(baseDir, dirName)
        saveDir.mkdirs()
    }

    @Override
    SoundProcessingState process(SoundBuffer soundBuffer) {

        for (int i = soundBuffer.start; i < soundBuffer.length; i += 1) {
            short correctedValue = soundBuffer.buffer[i] - noiseStatistics.offset
            short amplitude = Math.abs(correctedValue)

            if ( amplitude > noiseStatistics.threeSigma + noiseStatistics.max) {
                consecutiveSilenceSamples = 0
            }
            else {
                consecutiveSilenceSamples += 1
                if ( consecutiveSilenceSamples >= SOUND_END_TIME_THRESHOLD ) {
                    consecutiveSilenceSamples = 0

                    storeBytesForSaving(soundBuffer)
                    saveToFile()
                    soundBuffer.start = i
                    return SoundProcessingState.SILENCE
                }
            }
        }
        storeBytesForSaving(soundBuffer)
        soundBuffer.finished = true
        return SoundProcessingState.SOUND

    }

    def storeBytesForSaving(SoundBuffer soundBuffer) {
        byteOut.write(soundBuffer.bytes, soundBuffer.start * 2, (soundBuffer.length-soundBuffer.start) *2 )
    }

    def saveToFile() {
        byte[] audioBytes = byteOut.toByteArray()

        ByteArrayInputStream bais = new ByteArrayInputStream(audioBytes)
        long length = (audioBytes.length / frameSizeInBytes) - SOUND_END_TIME_THRESHOLD
        AudioInputStream audioInputStream = new AudioInputStream(bais, format, length)


        AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE

        String timestamp = timeFormat.format(new Date())

        String fileName = String.format("clip_%04d_%s.wav", saveCount, timestamp)
        File wavFile = new File(saveDir, fileName)


        AudioSystem.write(audioInputStream, fileType, wavFile)
        saveCount += 1
        byteOut = new ByteArrayOutputStream()
        log.debug("Saved sound clip: ${saveDir}/${fileName}")
    }


}
