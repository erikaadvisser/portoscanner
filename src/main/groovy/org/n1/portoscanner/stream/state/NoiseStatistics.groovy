package org.n1.portoscanner.stream.state

/**
 * This class is a container for statistics on the noise component of the sound.
 */
class NoiseStatistics {

    short offset
    short max
    short threeSigma
}
