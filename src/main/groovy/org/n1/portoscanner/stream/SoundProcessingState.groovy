package org.n1.portoscanner.stream

/**
 * This enum defines the states that exist in sound processing
 */
enum SoundProcessingState {

    START,
    SILENCE,
    SOUND
}