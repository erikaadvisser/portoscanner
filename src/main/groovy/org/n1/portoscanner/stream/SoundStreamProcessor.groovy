package org.n1.portoscanner.stream

import org.n1.portoscanner.stream.state.NoiseStatistics
import org.n1.portoscanner.stream.state.SilenceStateSoundProcessor
import org.n1.portoscanner.stream.state.SoundStateSoundProcessor
import org.n1.portoscanner.stream.state.StartStateSoundProcessor

import javax.sound.sampled.AudioFileFormat
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem

/**
 * This class processes an incoming stream of sound samples, parses them for non-noise and saves the non-noise to disk.
 */
class SoundStreamProcessor {

    short[] defaultBuffer

    SoundProcessingState state = SoundProcessingState.START
    def stateProcessors = [:]



//    ByteArrayOutputStream byteOut = new ByteArrayOutputStream()
//    boolean startedRecording = false
//    AudioFormat format
//    File saveDir
//    int frameSizeInBytes
//    int count = 0

    public SoundStreamProcessor(int bufferLength, AudioFormat format, int frameSizeInBytes, File saveDir) {
//        this.format = format
//        this.saveDir = saveDir
//        this.frameSizeInBytes = frameSizeInBytes

        defaultBuffer = new short[bufferLength / 2]

        NoiseStatistics noiseStatistics = new NoiseStatistics()

        stateProcessors[SoundProcessingState.START]   = new StartStateSoundProcessor(noiseStatistics)
        stateProcessors[SoundProcessingState.SILENCE] = new SilenceStateSoundProcessor(noiseStatistics)
        stateProcessors[SoundProcessingState.SOUND]   = new SoundStateSoundProcessor(noiseStatistics, format, frameSizeInBytes, saveDir)
    }

    public void processSamples(byte[] bufferAsBytes, int length) {
        SoundBuffer soundBuffer
        if ( length != bufferAsBytes.length) {
            short[] myBuffer = new short[length / 2]
            short[] buffer = convertToShorts(bufferAsBytes, myBuffer)
            byte[] rawBytes = Arrays.copyOf(bufferAsBytes, length)
            soundBuffer = new SoundBuffer(buffer, 0, rawBytes)
        }
        else {
            if (length == defaultBuffer.length * 2) {
                short[] buffer = convertToShorts(bufferAsBytes, defaultBuffer)
                soundBuffer = new SoundBuffer(buffer, 0, bufferAsBytes)
            }
            else {
                short[] myBuffer = new short[length / 2]
                short[] buffer = convertToShorts(bufferAsBytes, myBuffer)
                soundBuffer = new SoundBuffer(buffer, 0, bufferAsBytes)
            }
        }

        process(soundBuffer)
//        if (this.state == SoundProcessingState.SILENCE || this.state == SoundProcessingState.SOUND) {
//            byteOut.write(bufferAsBytes, 0, length)
//        }
//        if (this.state == SoundProcessingState.SILENCE && startedRecording && count == 0) {
//            byte[] audioBytes = byteOut.toByteArray()
//
//            ByteArrayInputStream bais = new ByteArrayInputStream(audioBytes)
//            long le = (audioBytes.length / frameSizeInBytes)
//            AudioInputStream audioInputStream = new AudioInputStream(bais, format, le)
//
//            AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE
//            File wavFile = new File(saveDir, "clip_all.wav")
//
//            AudioSystem.write(audioInputStream, fileType, wavFile)
//            byteOut = new ByteArrayOutputStream()
//            count += 1
//        }
//        if (this.state == SoundProcessingState.SOUND) {
//            startedRecording = true
//        }
    }

    public void process(SoundBuffer soundBuffer) {
        SoundStateProcessor stateProcessor = stateProcessors[state]

        SoundProcessingState newState = stateProcessor.process(soundBuffer)
        this.state = newState

        if ( !soundBuffer.finished) {
            process(soundBuffer)
        }
    }

    short[] convertToShorts(byte[] bytes, short[] buffer) {

        for (int i = 0; i < buffer.length; i += 1) {
            def mostSignificantByte = bytes[0 + i * 2]
            def leastSignificantByte = bytes[1 + i * 2]

            short val=(short)( ((mostSignificantByte&0xFF)<<8) | (leastSignificantByte&0xFF) );

            buffer[i] = val
        }
        return buffer
    }
}
