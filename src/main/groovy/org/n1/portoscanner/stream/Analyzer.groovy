package org.n1.portoscanner.stream

import org.apache.commons.math3.stat.descriptive.SummaryStatistics

/**
 * This class is a testing class to find out how to filter for actual content.
 */
class Analyzer {

    /** the standard deviation is about 8.6 */
    private static final short THREE_SIGMA = 26

    private static final short NOISE_MAX = 34

    /** The amount of samples that have to be higher than (NOISE_MAX + THREE SIGMA) to confirm start of sound) */
    private static final int SOUND_START_TIME_THRESHOLD = 10

    /** 44 khz, so the threshold for detecting silence is 25 ms */
    private static final int SOUND_END_TIME_THRESHOLD = 1100

    public static void main(String... args) {
        new Analyzer().run()
    }

    def run()  {
        byte[] rawBytes = readRawBytes("raw2.dat")
        short[] rawShorts = toShorts(rawBytes)

        short[] startSilence = rawShorts[0..10000]
        def stats = new SummaryStatistics()
        startSilence.each { it -> stats.addValue(it) }
        def offset = stats.getMean()

        println "Offset: ${offset}"

        for (int i = 0; i< rawShorts.length; i++) {
            short s = rawShorts[i]
            rawShorts[i] =  (short)Math.abs((short)(s - offset))
        }
        def shorts = rawShorts;


        int startOfSound = findStart(shorts)
        println("Sound starts at ${toMs(startOfSound)}")

        int stopSound = findStop(shorts, startOfSound)
        println("Sound stops at ${toMs(stopSound)}")




        println("mean = ${offset}")
        def sigma = stats.getStandardDeviation()
        println("sigma = ${sigma}")

//        def shorts = rawShorts.collect { (short)(it) }

        def silence2 = shorts[0..50000]

        def stats2 = new SummaryStatistics()
        silence2.each { it -> stats2.addValue(it) }

        def offset2 = stats2.getMean()
        println("mean2 = ${(short)offset2}")
        def sigma2 = stats2.getStandardDeviation()
        println("sigma2 = ${sigma2}")
        def max = silence2.max()
        def min = silence2.min()
        println("max = ${max}, min = ${min}")

        def count = silence2.count { it -> (Math.abs(it) > 4 * sigma2 )}
        println("count = ${count}")

        println "${shorts.size()} shorts read."
        FileWriter writer = new FileWriter("amplitude.txt")
        shorts.each {
            it -> writer.write("${it}\n")
//            it -> writer.write("${Math.abs(it)}\n")
        }
        writer.close();
        println "wrote amp file."
    }

    String toMs(int position) {
        float seconds = (float)position / 44000.0
        return String.format("%.3fs", seconds)
    }

    int findStart(short[] shorts) {
//        DescriptiveStatistics stats = new DescriptiveStatistics(10)

        int consequtiveSamples = 0;

        for (int i = 0; i < shorts.length; i++) {
            short s = Math.abs(shorts[i])
//            stats.addValue(s)

//            short average = stats.getMean()
//            short value = s - average

            if ( s > THREE_SIGMA + NOISE_MAX) {
                consequtiveSamples += 1
                if (consequtiveSamples >= SOUND_START_TIME_THRESHOLD) {
                    return i - consequtiveSamples;
                }
            }
            else {
                consequtiveSamples = 0;
            }
        }
        throw new RuntimeException("Failed to find start of sound")
    }

    int findStop(short[] shorts, int start) {
        int countSilence = 0;
        for (int i = start; i < shorts.length; i++) {
            short s = shorts[i]

            if ( s < THREE_SIGMA + NOISE_MAX) {
                countSilence += 1
                if (countSilence >= SOUND_END_TIME_THRESHOLD) {
                    return i - countSilence;
                }
            }
            else {
                countSilence = 0;
            }
        }
        throw new RuntimeException("Failed to find start of sound")

    }

    byte[] readRawBytes(String fileName) {
        def input = getClass().getClassLoader().getResourceAsStream(fileName)
        def baos = new ByteArrayOutputStream()

        baos << input

        input.close()
        return baos.toByteArray()
    }

    short[] toShorts(byte[] bytes) {
        def shorts = new short[bytes.length / 2]
        for (int i = 0; i < shorts.length; i += 1) {
            def mostSignificantByte = bytes[0 + i * 2]
            def leastSignificantByte = bytes[1 + i * 2]

            short val=(short)( ((mostSignificantByte&0xFF)<<8) | (leastSignificantByte&0xFF) );

            shorts[i] = val
        }
        return shorts
    }


    short determineOffset(short[] shorts) {
        long sum = shorts[0..50000].sum();
        double avarage = sum / 50000;

        return (short)avarage
    }

    short determineSigma(short[] shorts) {

    }


}
