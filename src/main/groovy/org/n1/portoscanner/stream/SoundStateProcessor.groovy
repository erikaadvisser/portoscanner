package org.n1.portoscanner.stream

/**
 * This interface defines a processor for a specific state of processing
 */
interface SoundStateProcessor {

    /**
     * Process (part of) the buffer.
     *
     * @param where in the buffer the processing shall start
     * @return the number of samples (shorts) processed.
     */
    public SoundProcessingState process(SoundBuffer soundBuffer);


}