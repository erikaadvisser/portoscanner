package org.n1.portoscanner.web.controler;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class
 */
@Controller
public class ConfigController {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String showIndex(Model model) {
        model.addAttribute("val", "Erik");
        return "test.jsp";
    }
}


