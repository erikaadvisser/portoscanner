package org.n1.portoscanner.web.controler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("UnusedDeclaration")
public class IndexController {

//    @Value("${example.message}")
    private String message = "msg";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String[] showIndex() {
        return new String[] {message};
    }

}
