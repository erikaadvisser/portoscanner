package org.n1.portoscanner;

import javax.sound.sampled.*;

public class MixerChooser {

    public static void main(String... args) throws LineUnavailableException {

        Mixer.Info[] mixerInfos = AudioSystem.getMixerInfo();

        System.out.println("Playback devices:\n");
        for (Mixer.Info info: mixerInfos) {
            if (info.getDescription().contains("Playback")) {
                System.out.println(info.getName() + " - " + info.getDescription());
            }
        }
        System.out.println("\nRecording devices:\n");
        for (Mixer.Info info: mixerInfos) {
            if (info.getDescription().contains("Capture")) {
                System.out.println(info.getName() + " - " + info.getDescription());
            }
        }

//        SourceDataLine playbackLine = getPlaybackSourceLine();
//        TargetDataLine recordLine = getRecordTargetLine();

    }

    public static SourceDataLine getPlaybackSourceLine() throws LineUnavailableException {
        String name="C-Media USB";
        Mixer.Info playbackInfo = getPlaybackMixer(name);
        System.out.println(playbackInfo.getName() + " - " + playbackInfo.getDescription());

        Mixer playback = AudioSystem.getMixer(playbackInfo);
        AudioFormat format = getAudioFormat();
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
        SourceDataLine line = (SourceDataLine)playback.getLine(info);
        return line;
    }


    public static TargetDataLine getRecordTargetLine() throws LineUnavailableException {
        String name="C-Media USB";
        Mixer.Info recordInfo = getRecordMixer(name);
        System.out.println(recordInfo.getName() + " - " + recordInfo.getDescription());

        Mixer record = AudioSystem.getMixer(recordInfo);
        AudioFormat format = getAudioFormat();
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        TargetDataLine line = (TargetDataLine)record.getLine(info);
        return line;
    }

    public static AudioFormat getAudioFormat() {
        AudioFormat.Encoding encoding = AudioFormat.Encoding.PCM_SIGNED;
        float rate = 44100.0f;
        int channels = 1;
        int frameSize = 2;
        int sampleSize = 16;
        boolean bigEndian = true;

        AudioFormat format = new AudioFormat(encoding, rate, sampleSize, channels, (sampleSize / 8)
                * channels, rate, bigEndian);


        return format;
    }

    private static Mixer.Info getRecordMixer(String name) {
        String type="Capture";
        return getMixerOfType(name, type);
    }

    private static Mixer.Info getPlaybackMixer(String name) {
        String type="Playback";
        return getMixerOfType(name, type);
    }

    private static Mixer.Info getMixerOfType(String name, String type) {
        Mixer.Info[] mixerInfos = AudioSystem.getMixerInfo();
        for (Mixer.Info info: mixerInfos) {
            if ( info.getDescription().contains(type) && info.getName().contains(name)) {
                return info;
            }
        }
        throw new RuntimeException("Mixer not found with name \"" + name + "\", and type: " + type);
    }

}
