package org.n1.portoscanner.analysis;

/**
 * This class performs initial analysis on existing files to try to find out when audio is interesting
 */
public class Analyzer {

    public static void main(String... args) {
        new Analyzer().run();
    }

    private void run() {
        byte[] rawBytes = readRawBytes();
    }

    private byte[] readRawBytes() {
        getClass().getClassLoader().getResourceAsStream("raw.dat");
        return null;
    }
}
