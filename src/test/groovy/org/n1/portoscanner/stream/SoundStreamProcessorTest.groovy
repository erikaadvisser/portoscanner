package org.n1.portoscanner.stream

import org.n1.portoscanner.stream.state.NoiseStatistics
import org.n1.portoscanner.stream.state.SoundStateSoundProcessor
import org.n1.portoscanner.stream.state.StartStateSoundProcessor
import spock.lang.Specification

import javax.sound.sampled.AudioFormat

/**
 * This class is a Spock test for {@link SoundStreamProcessor}
 */
class SoundStreamProcessorTest extends Specification {

    SoundStreamProcessor processor

    File baseDir
    int bufferLengthInBytes = 11024
    Random random = new Random()


    Closure<Short> silenceGenerator = {
        short s = 10 + random.nextFloat() * 20
        return s
    }

    int soundCounter = 0;

    Closure<Short> soundGenerator = {
        short s = 10 + random.nextFloat() * 20 + soundCounter * 100
        soundCounter += 1
        return s
    }

    def setup() {
        AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, true)
        int frameSizeInBytes = 2
        baseDir = File.createTempDir()


        processor = new SoundStreamProcessor(bufferLengthInBytes, format, frameSizeInBytes, baseDir)
    }

    def cleanup() {
        String path = baseDir.getAbsolutePath().toLowerCase()
        if (path.contains("temp") || path.contains("tmp")) {
            removeRecursive(baseDir)
        }
        else {
            println "Temp dir in a suspect place, not deleting it: '${path}'"
            System.exit(1)
        }
    }

    def removeRecursive(File file) {
        if (file.isFile()) {
            file.delete()
        }
        else {
            file.listFiles().each { removeRecursive(it)}
            file.delete()
        }
    }

    byte[] create_silence(int length) {
        return createBuffer(length, silenceGenerator)
    }

    byte[] create_sound(int length) {
        return createBuffer(length, soundGenerator)
    }

    byte[] createBuffer(int length, Closure<Short> generator) {
        def byteIn = new ByteArrayOutputStream()
        for (int i = 0; i < length; i++) {
            short s = generator.call()
            byte b1 = (byte)((s >> 8) & 0xff);
            byte b2 = (byte)(s & 0xff);
            byteIn.write(b1)
            byteIn.write(b2)
        }
        return byteIn.toByteArray()
    }



    def "test full default buffer"() {
        given:
        byte[] buffer = new byte[bufferLengthInBytes]

        when:
        processor.processSamples(buffer, bufferLengthInBytes)

        then:
        processor.state == SoundProcessingState.START
        ((StartStateSoundProcessor)processor.stateProcessors[SoundProcessingState.START]).totalSamplesRead == 5512
    }

    def "test partial default buffer"() {
        given:
        byte[] buffer = new byte[bufferLengthInBytes]

        when:
        processor.processSamples(buffer, 10)

        then:
        processor.state == SoundProcessingState.START
        ((StartStateSoundProcessor)processor.stateProcessors[SoundProcessingState.START]).totalSamplesRead == 5
    }

    def "test different size buffer"() {
        given:
        byte[] buffer = new byte[100]

        when:
        processor.processSamples(buffer, 100)

        then:
        processor.state == SoundProcessingState.START
        ((StartStateSoundProcessor)processor.stateProcessors[SoundProcessingState.START]).totalSamplesRead == 50
    }

    def "test partial different size buffer"() {
        given:
        byte[] buffer = new byte[100]

        when:
        processor.processSamples(buffer, 50)

        then:
        processor.state == SoundProcessingState.START
        ((StartStateSoundProcessor)processor.stateProcessors[SoundProcessingState.START]).totalSamplesRead == 25
    }

    def "Test scenario process start"() {
        given:
        byte[] b1 = create_silence(bufferLengthInBytes)
        byte[] b2 = create_silence(StartStateSoundProcessor.MINIMUM_SILENCE_SAMPLES)

        when:
        processor.processSamples(b1, b1.length)
        processor.processSamples(b2, b2.length)

        then:
        processor.state == SoundProcessingState.SILENCE
    }

    def "Test scenario process start-silence-sound"() {
        given:
        byte[] b1 = create_silence(bufferLengthInBytes)
        byte[] b2 = create_silence(StartStateSoundProcessor.MINIMUM_SILENCE_SAMPLES)
        byte[] b3 = create_silence(bufferLengthInBytes)
        byte[] b4 = create_silence(bufferLengthInBytes)
        byte[] b5 = create_silence(bufferLengthInBytes)
        byte[] b6 = create_sound(bufferLengthInBytes)
        byte[] b7 = create_sound(bufferLengthInBytes)
        byte[] b8 = create_silence(bufferLengthInBytes)
        byte[] b9 = create_sound(bufferLengthInBytes)
        byte[] b10 = create_silence(bufferLengthInBytes)

        when:
        processor.processSamples(b1, b1.length)
        processor.processSamples(b2, b2.length)
        processor.processSamples(b3, b3.length)
        processor.processSamples(b4, b4.length)
        processor.processSamples(b5, b5.length)
        processor.processSamples(b6, b6.length)
        processor.processSamples(b7, b7.length)
        processor.processSamples(b8, b8.length)
        processor.processSamples(b9, b9.length)
        processor.processSamples(b10, b10.length)

        then:
        processor.state == SoundProcessingState.SILENCE
        ((SoundStateSoundProcessor)processor.stateProcessors[SoundProcessingState.SOUND]).saveCount == 2
    }

}
