package org.n1.portoscanner.stream.state

import org.n1.portoscanner.stream.SoundBuffer
import org.n1.portoscanner.stream.SoundProcessingState
import spock.lang.Specification

import javax.sound.sampled.AudioFormat

/**
 * This class is a Spock test for {@link SoundStateSoundProcessor}
 */
class SoundStateSoundProcessorTest extends Specification {

    NoiseStatistics noiseStats
    SoundStateSoundProcessor processor
    File baseDir

    def setup() {
        noiseStats = new NoiseStatistics(
                offset: 10,
                max: 36,
                threeSigma: 8
        )
        // These numbers the silence threshold is 60 ( 36+8+8+8 ) after offset calibration.
        // This means that values of 71 (-10 offset) or -51 (-10 offset) will qualify.

        AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, true)
        int frameSizeInBytes = 2
        baseDir = File.createTempDir()

        processor = new SoundStateSoundProcessor(noiseStats, format, frameSizeInBytes, baseDir)
    }

    def cleanup() {
        String path = baseDir.getAbsolutePath().toLowerCase()
        if (path.contains("temp") || path.contains("tmp")) {
            removeRecursive(baseDir)
        }
        else {
            println "Temp dir in a suspect place, not deleting it."
            System.exit(1)
        }
    }

    def removeRecursive(File file) {
        if (file.isFile()) {
            file.delete()
        }
        else {
            file.listFiles().each { removeRecursive(it)}
            file.delete()
        }
    }

    SoundBuffer createSoundBuffer(int value, int samples) {
        short[] buffer = new short[samples]
        buffer.eachWithIndex { short entry, int index -> buffer[index] = value}
        byte[] bytes = new byte[samples * 2]
        bytes.eachWithIndex { byte entry, int index -> bytes[index] = value }

        return new SoundBuffer( buffer, 0, bytes )
    }

    def "Test creation of save dir"() {
        given:
        Date now = new Date()
        String dirName = now.format("yyyy-MM-dd_HH-mm-ss")

        when:
        File dir = baseDir.listFiles()[0]

        then:
        dir.getName() == dirName
    }

    def "Test storage of sound input"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(100, 10)

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SOUND
        soundBuffer.finished == true
        processor.byteOut.size() == 20
        processor.consecutiveSilenceSamples == 0
    }

    def "Test honor start value"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(0, 10)
        soundBuffer.start = 5

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SOUND
        soundBuffer.finished == true
        processor.byteOut.size() == 10
        processor.consecutiveSilenceSamples == 5
    }

    def "Test don't find silence when silence period is short"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(50, 1200)
        soundBuffer.buffer[0] = 100
        soundBuffer.buffer[999] = 100

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SOUND
        soundBuffer.finished == true
        processor.byteOut.size() == 2400
        processor.consecutiveSilenceSamples == 200
    }

    def "Test save short clip"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(50, 1200)

        // some values to make the wav file a bit interesting. Not really part of the test.
        for (int i = 0; i < 2000; i += 2) {
            soundBuffer.bytes[i] = i + 100
            soundBuffer.bytes[i+1] = 0
        }
        soundBuffer.buffer[5] = 100

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SILENCE
        soundBuffer.finished == false
        soundBuffer.start == 1105
    }
}
