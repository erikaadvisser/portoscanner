package org.n1.portoscanner.stream.state

import org.n1.portoscanner.stream.SoundBuffer
import org.n1.portoscanner.stream.SoundProcessingState
import spock.lang.Specification

/**
 * This class is a Spock test for {@link StartStateSoundProcessor}.
 */
class StartStateSoundProcessorTest extends Specification {

    NoiseStatistics noiseStats
    StartStateSoundProcessor processor

    def setup() {
        noiseStats = new NoiseStatistics()
        processor = new StartStateSoundProcessor(noiseStats)
    }


    SoundBuffer createSoundBuffer(int value) {
        short[] buffer = new short[StartStateSoundProcessor.MINIMUM_SILENCE_SAMPLES]
        buffer.eachWithIndex { short entry, int index -> buffer[index] = value}

        return new SoundBuffer( buffer, 0, null )
    }


    def "Test happy flow"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(40)

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SILENCE
        noiseStats.offset == 40
    }


    def "Test happy flow in two buffers"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(0)
        soundBuffer.start = 50000

        SoundBuffer soundBuffer2 = createSoundBuffer(10)
        soundBuffer2.start = 49999 // rounding floats to shorts is stupid, need to add an extra sample to make it add up to an average of 5.

        when:
        SoundProcessingState state1 = processor.process(soundBuffer)
        SoundProcessingState state2 = processor.process(soundBuffer2)

        then:
        state1 == SoundProcessingState.START
        soundBuffer.finished == true

        state2 == SoundProcessingState.SILENCE
        soundBuffer2.finished == true
        noiseStats.offset == 5
    }

    def "Test rejection of invalid noise segment - too extreme individual value"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(0)
        soundBuffer.buffer[55] = -60

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.START
    }

    def "Test rejection of invalid noise segment - too extreme variance"() {
        given:
        Random random = new Random()
        SoundBuffer soundBuffer = createSoundBuffer(0)
        for (int i = 0; i < soundBuffer.buffer.length; i++) {
            soundBuffer.buffer[i] = (short)((random.nextFloat() * 80) - 40)
        }

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.START
    }

}
