package org.n1.portoscanner.stream.state

import org.n1.portoscanner.stream.SoundBuffer
import org.n1.portoscanner.stream.SoundProcessingState
import spock.lang.Specification

/**
 * This class defines a Spock test for {@link SilenceStateSoundProcessor}.
 */
class SilenceStateSoundProcessorTest extends Specification {

    NoiseStatistics noiseStats
    SilenceStateSoundProcessor processor

    def setup() {
        noiseStats = new NoiseStatistics(
                offset: 10,
                max: 36,
                threeSigma: 8
        )
        // These numbers the silence threshold is 60 ( 36+8+8+8 ) after offset calibration.
        // This means that values of 71 (-10 offset) or -51 (-10 offset) will qualify.

        processor = new SilenceStateSoundProcessor(noiseStats)
    }

    SoundBuffer createSoundBuffer(int value, int samples) {
        short[] buffer = new short[samples]
        buffer.eachWithIndex { short entry, int index -> buffer[index] = value}

        return new SoundBuffer( buffer, 0, null )
    }

    def "Test continue parsing when not enough data is supplied"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(100, 5)

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SILENCE
        soundBuffer.finished == true
    }

    def "Test happy flow positive numbers"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(71, 10)

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SOUND
        soundBuffer.finished == false
        soundBuffer.start == 9
    }

    def "Test happy flow negative numbers"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(-51, 10)

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SOUND
        soundBuffer.finished == false
        soundBuffer.start == 9
    }

    def "Test ignore too short sound spikes"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(0, 10)

        soundBuffer.buffer[6] = 100
        soundBuffer.buffer[7] = 100
        soundBuffer.buffer[8] = 100

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SILENCE
        soundBuffer.finished == true
        processor.consecutiveSamples == 0
    }

    def "Test honor start value"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(100, 10)
        soundBuffer.start = 2


        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SILENCE
        soundBuffer.finished == true
    }

    def "Test honor find sound in middle of buffer"() {
        given:
        SoundBuffer soundBuffer = createSoundBuffer(100, 20)
        soundBuffer.start = 2

        when:
        SoundProcessingState state = processor.process(soundBuffer)

        then:
        state == SoundProcessingState.SOUND
        soundBuffer.finished == false
        soundBuffer.start == 11
    }
}
